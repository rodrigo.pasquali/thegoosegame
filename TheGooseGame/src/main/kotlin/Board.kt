import exception.BoardSpacePositionOutOfBoundException
import spaces.*

class Board {
    fun showRulesSpaces() {
        for(i in 1..Board.QUANTITY_SPACES) {
            println(getMessageSpaceAction(i))
        }
    }

    fun getMessageSpaceAction(spacePosition : Int) : String{
        validatePosition(spacePosition)

        var space = initSpace(spacePosition)

        return space.getMessagePosition()
    }

    private fun initSpace(spacePosition: Int): Space {
        return SpacesFactory().create(spacePosition)
    }

    private fun validatePosition(spacePosition : Int) {
        if(spacePosition <= 0) {
            throw BoardSpacePositionOutOfBoundException()
        }
    }

    companion object {
        const val QUANTITY_SPACES : Int = 63
    }
}