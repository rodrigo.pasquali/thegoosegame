import spaces.*

class SpacesFactory {
    fun create(position: Int): Space {
        if (isBridgeSpace(position)) return BridgeSpace(position)
        if (isHotelSpace(position)) return HotelSpace(position)
        if (isWellSpace(position)) return WellSpace(position)
        if (isMazeSpace(position)) return MazeSpace(position)
        if (isPrisonSpace(position)) return PrisonSpace(position)
        if (isTwoFowardPositionSpace(position)) return TwoFowardPositionSpace(position)
        if (isDeathSpace(position)) return DeathSpace(position)
        if (isFinishSpace(position)) return FinishSpace(position)
        if (isBeyondSpace(position)) return BeyondSpace(position)

        return NormalSpace(position)
    }

    private fun isBridgeSpace(spacePosition : Int) : Boolean{
        return (spacePosition == BRIDGE_SPACE)
    }

    private fun isHotelSpace(spacePosition : Int) : Boolean{
        return (spacePosition == HOTEL_SPACE)
    }

    private fun isWellSpace(spacePosition : Int) : Boolean{
        return (spacePosition == WELL_SPACE)
    }

    private fun isMazeSpace(spacePosition : Int) : Boolean{
        return (spacePosition == MAZE_SPACE)
    }

    private fun isTwoFowardPositionSpace(spacePosition : Int) : Boolean{
        return (((spacePosition).mod(6) == 0)  && (spacePosition != BRIDGE_SPACE))
    }

    private fun isPrisonSpace(spacePosition : Int) : Boolean{
        return (spacePosition in PRISON_BEGINNING_SPACE.rangeTo(PRISON_ENDING_SPACE))
    }

    private fun isDeathSpace(spacePosition : Int) : Boolean{
        return (spacePosition == DEATH_SPACE)
    }

    private fun isFinishSpace(spacePosition : Int) : Boolean{
        return (spacePosition == FINISH_SPACE)
    }

    private fun isBeyondSpace(spacePosition : Int) : Boolean{
        return (spacePosition > FINISH_SPACE)
    }

    private companion object {
        const val BRIDGE_SPACE : Int = 6
        const val HOTEL_SPACE : Int = 19
        const val WELL_SPACE : Int = 31
        const val MAZE_SPACE : Int = 42
        const val PRISON_BEGINNING_SPACE : Int = 50
        const val PRISON_ENDING_SPACE : Int = 55
        const val DEATH_SPACE : Int = 58
        const val FINISH_SPACE : Int = 63
    }
}