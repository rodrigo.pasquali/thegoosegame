package exception

import java.lang.RuntimeException

class BoardSpacePositionOutOfBoundException : RuntimeException(){}