package spaces

class BeyondSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "Move to space 53 and stay in prison for two turns"
    }
}
