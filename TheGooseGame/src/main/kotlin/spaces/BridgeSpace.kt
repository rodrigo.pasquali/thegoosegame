package spaces

class BridgeSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "The Bridge: Go to space ${positionSpace + 6}"
    }
}