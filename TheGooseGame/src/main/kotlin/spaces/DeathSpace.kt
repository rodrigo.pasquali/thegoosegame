package spaces

class DeathSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "Death: Return your piece to the beginning - start the game again"
    }
}
