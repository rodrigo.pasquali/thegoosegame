package spaces

class FinishSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "Finish: you ended the game"
    }
}
