package spaces

class HotelSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "The Hotel: Stay for (miss) one turn"
    }
}