package spaces

class MazeSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "The Maze: Go back to space 39"
    }
}