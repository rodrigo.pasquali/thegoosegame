package spaces

class NormalSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "Stay in space $positionSpace"
    }
}