package spaces

class PrisonSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "The Prison: Wait until someone comes to release you - they then take your place"
    }
}