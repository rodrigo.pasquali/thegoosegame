package spaces

interface Space {
    val positionSpace : Int

    fun getMessagePosition() : String
}