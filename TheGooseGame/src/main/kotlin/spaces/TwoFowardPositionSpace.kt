package spaces

class TwoFowardPositionSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "Move two spaces forward"
    }
}