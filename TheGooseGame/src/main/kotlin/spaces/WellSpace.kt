package spaces

class WellSpace(override val positionSpace: Int) : Space {
    override fun getMessagePosition() : String {
        return "The Well: Wait until someone comes to pull you out - they then take your place"
    }
}