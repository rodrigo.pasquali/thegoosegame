import exception.BoardSpacePositionOutOfBoundException
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TheGooseGameTest {
    lateinit var board : Board

    @BeforeTest
    fun setup() {
        board = Board()
    }

    @Test
    fun `position 0 on the board should throw exception BoardSpacePositionOutOfBoundException`() {
        var position : Int = 0

        assertFailsWith<BoardSpacePositionOutOfBoundException>(
            block = {
                board.getMessageSpaceAction(position)
            }
        )
    }

    @Test
    fun `position beyond the space 63 should be a BeyondSpace`() {
        var position : Int = 64

        assertEquals("Move to space 53 and stay in prison for two turns", board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 6 on the board should be a BridgeSpace`() {
        var position : Int = 6

        assertEquals("The Bridge: Go to space 12", board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 18 on the board should be a TwoFowardPositionSpace`() {
        var position : Int = 18

        assertEquals("Move two spaces forward", board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 1 on the board should be a NormalSpace`() {
        var position : Int = 1

        assertEquals("Stay in space $position", board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 63 on the board should be a FinishSpace`() {
        var position : Int = 63

        assertEquals("Finish: you ended the game", board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 19 on the board should be a HotelSpace`() {
        var position : Int = 19

        assertEquals("The Hotel: Stay for (miss) one turn", board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 31 on the board should be a WellSpace`() {
        var position : Int = 31

        assertEquals("The Well: Wait until someone comes to pull you out - they then take your place",
            board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 42 on the board should be a MazeSpace`() {
        var position : Int = 42

        assertEquals("The Maze: Go back to space 39",
            board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 50 on the board should be a PrisonSpace`() {
        var position : Int = 50

        assertEquals("The Prison: Wait until someone comes to release you - they then take your place",
            board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 55 on the board should be a PrisonSpace`() {
        var position : Int = 55

        assertEquals("The Prison: Wait until someone comes to release you - they then take your place",
            board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 53 on the board should be a PrisonSpace`() {
        var position : Int = 53

        assertEquals("The Prison: Wait until someone comes to release you - they then take your place",
            board.getMessageSpaceAction(position))
    }

    @Test
    fun `position 58 on the board should be a DeathSpace`() {
        var position : Int = 58

        assertEquals("Death: Return your piece to the beginning - start the game again",
            board.getMessageSpaceAction(position))
    }
}